﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using LVL2_ASPNet_MVC_05.Models;

namespace LVL2_ASPNet_MVC_05.Controllers
{
    public class DataController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("api/data/forall")]
        //public IHttpActionResult Get()
        public IEnumerable<tbl_ms_user_new> Get() // set database
        {
            TESTAPIEntities1 entities = new TESTAPIEntities1();
            return entities.tbl_ms_user_new.ToList();
            //return Ok("Now server time is: " + DateTime.Now.ToString());
        }

        [Authorize]
        [HttpGet]
        [Route("api/data/authenticate")]
        public IHttpActionResult GetForAuthenticate()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("Hello " + identity.Name);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("api/data/authorize")]
        //public IHttpActionResult GetForAdmin()
        public IEnumerable<tbl_ms_user_new> GetForAdmin()
        {
            TESTAPIEntities1 entities = new TESTAPIEntities1();
            var identity = (ClaimsIdentity)User.Identity;
            var roles = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);
            return entities.tbl_ms_user_new.ToList();
            //return Ok("Hello " + identity.Name + " Role: " + string.Join(",", roles.ToList()));
        }
    }
}
